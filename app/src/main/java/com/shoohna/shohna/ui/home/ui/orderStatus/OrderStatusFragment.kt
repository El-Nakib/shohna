package com.shoohna.shohna.ui.home.ui.orderStatus

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentOrderStatusBinding
import com.shoohna.shohna.ui.home.ui.checkOutProcess.reciet.RecietModel
import com.shoohna.shohna.ui.home.ui.checkOutProcess.reciet.RecietRecyclerViewAdapter

/**
 * A simple [Fragment] subclass.
 */
class OrderStatusFragment : Fragment() {
    lateinit var binding: FragmentOrderStatusBinding

    var orderStatuesList = ArrayList<RecietModel>()
    lateinit var orderStatuesAdapter: RecietRecyclerViewAdapter
//    private var stepList = ArrayList<StepBean>()
//
//    private var step1 : StepBean = StepBean("Cart",1)
//    private var step2 : StepBean = StepBean("Shipping",1)
//    private var step3 : StepBean = StepBean("Payment",0)
//    private var step4 : StepBean = StepBean("Reciet",-1)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentOrderStatusBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner

        orderStatuesAdapter= RecietRecyclerViewAdapter(orderStatuesList, context?.applicationContext)
        binding.orderStatusRecycvlerViewId.adapter = orderStatuesAdapter
//        setUpStepView()
        addData()

        return binding.root
    }

//    fun setUpStepView()
//    {
//        stepList.add(step1)
//        stepList.add(step2)
//        stepList.add(step3)
//        stepList.add(step4)
//
//        binding.stepViewId.setStepViewTexts(stepList).setTextSize(13)
//            .setStepsViewIndicatorCompletedLineColor(resources.getColor(R.color.pink))
//            .setStepsViewIndicatorUnCompletedLineColor(resources.getColor(R.color.grey))
//            .setStepViewComplectedTextColor(resources.getColor(R.color.pink))
//            .setStepViewUnComplectedTextColor(resources.getColor(R.color.pink))
//            .setStepsViewIndicatorCompleteIcon(resources.getDrawable(R.drawable.ic_step_completed))
//            .setStepsViewIndicatorDefaultIcon(resources.getDrawable(R.drawable.ic_step_after))
//            .setStepsViewIndicatorAttentionIcon(resources.getDrawable(R.drawable.ic_step_current))
//    }
    private fun addData() {
        orderStatuesList.add(RecietModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress",235.3f,4))
        orderStatuesList.add(RecietModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress",235.3f,4))
        orderStatuesList.add(RecietModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress",235.3f,4))
        orderStatuesList.add(RecietModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress",235.3f,4))
        orderStatuesList.add(RecietModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress",235.3f,4))
        orderStatuesList.add(RecietModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress",235.3f,4))
        orderStatuesAdapter.notifyDataSetChanged()

    }

}
