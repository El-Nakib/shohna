package com.shoohna.shohna.ui.welcome.ui.welcomeFragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentWelcomeBinding
import com.shoohna.shohna.ui.home.MainActivity

/**
 * A simple [Fragment] subclass.
 */
class WelcomeFragment : Fragment() {
    lateinit var binding: FragmentWelcomeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentWelcomeBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner

        binding.signInBtnId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_welcomeFragment_to_loginFragment) }
        }

        binding.signUpBtnId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_welcomeFragment_to_registerFragment) }
        }

        binding.skipTxtViewId.setOnClickListener {
            startActivity(Intent(activity, MainActivity::class.java))
            activity?.finish()
        }

        return binding.root    }

}
