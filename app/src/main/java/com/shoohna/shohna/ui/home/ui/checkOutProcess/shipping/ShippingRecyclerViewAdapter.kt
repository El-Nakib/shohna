package com.shoohna.shohna.ui.home.ui.checkOutProcess.shipping

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.shohna.databinding.ShippingInformationItemRowBinding

class ShippingRecyclerViewAdapter  (private var dataList: List<ShippingModel>, private val context: Context?) : RecyclerView.Adapter<ShippingRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ShippingInformationItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = dataList[position]
        holder.bind(dataModel)

    }


    class ViewHolder(private var binding: ShippingInformationItemRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ShippingModel) {
            binding.model = item
            binding.executePendingBindings()

        }

    }
}