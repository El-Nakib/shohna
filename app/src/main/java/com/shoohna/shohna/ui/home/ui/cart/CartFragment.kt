package com.shoohna.shohna.ui.home.ui.cart

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.shoohna.shohna.ui.home.ui.cart.CartModel
import com.shoohna.shohna.ui.home.ui.cart.CartRecyclerViewAdapter
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentCartBinding

/**
 * A simple [Fragment] subclass.
 */
class CartFragment : Fragment() {
    lateinit var binding: FragmentCartBinding
    var cartList = ArrayList<CartModel>()
    lateinit var cartAdapter: CartRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentCartBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner


        cartAdapter= CartRecyclerViewAdapter(cartList, context?.applicationContext)
        binding.cartRecyclerViewId.adapter = cartAdapter

        binding.checkOutBtnId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_cartFragment_to_checkoutProcessFragment) }
            activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE

        }

        binding.statusBtnId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_cartFragment_to_orderStatusFragment) }
            activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE

        }

        addData()


        return binding.root
    }

    private fun addData() {
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartAdapter.notifyDataSetChanged()

    }

}
