package com.shoohna.shohna.ui.welcome.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentLoginBinding

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {
    lateinit var binding: FragmentLoginBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

        binding.signUpTxtViewId.setOnClickListener {
            view?.let { Navigation.findNavController(it).navigate(R.id.action_loginFragment_to_registerFragment) }
        }

        binding.forgetPasswordTxtId.setOnClickListener {
            view?.let { Navigation.findNavController(it).navigate(R.id.action_loginFragment_to_forgetPasswordFragment) }

        }

        return  binding.root

    }

}
