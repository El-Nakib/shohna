package com.shoohna.shohna.ui.home.ui.product

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shoohna.shohna.ui.home.ui.product.ProductFragmentArgs
import com.shoohna.shohna.databinding.FragmentProductBinding
import com.shoohna.shohna.pojo.model.ProductListModel
import com.shoohna.shohna.ui.home.ui.home.ProductListRecyclerViewAdapter
import com.squareup.picasso.Picasso

/**
 * A simple [Fragment] subclass.
 */
class ProductFragment : Fragment() {
    lateinit var binding:FragmentProductBinding
    var productViewList = ArrayList<ProductListModel>()
    lateinit var productViewAdapter: ProductViewRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentProductBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner
//        Picasso.get().load("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTF3uiLXJ65UrAvjG6vUIVWJ3gVZVjj8yvOO51mI3sAFooiUDS3").into(binding.circleImageView)

//        arguments?.let { val safeArgs =
//            ProductFragmentArgs.fromBundle(it)
//            binding.productTypeTxtView.text = safeArgs.productType
//        }
//
//        productViewAdapter= ProductViewRecyclerViewAdapter(productViewList, context?.applicationContext)
//        binding.productViewRecyclerViewId.adapter = productViewAdapter

//        binding.btnBackId.setOnClickListener {
//            activity?.onBackPressed()
//        }

        addData()

        return binding.root
    }

    fun addData()
    {
        productViewList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false))
        productViewList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false))
        productViewList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false))
        productViewList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false))
        productViewList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false))
        productViewAdapter.notifyDataSetChanged()
    }

}
