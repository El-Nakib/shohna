package com.shoohna.shohna.ui.home.ui.checkOutProcess.reciet

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.shohna.databinding.RecietItemRowBinding

class RecietRecyclerViewAdapter (private var dataList: List<RecietModel>, private val context: Context?) : RecyclerView.Adapter<RecietRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            RecietItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = dataList[position]
        holder.bind(dataModel)

    }


    class ViewHolder(private var binding: RecietItemRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: RecietModel) {
            binding.model = item
            binding.executePendingBindings()

        }

    }
}
