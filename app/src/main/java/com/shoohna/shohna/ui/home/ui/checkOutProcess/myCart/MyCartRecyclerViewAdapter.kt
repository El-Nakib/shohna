package com.shoohna.shohna.ui.home.ui.checkOutProcess.myCart

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.shohna.databinding.CheckoutMycartItemRowBinding
import com.shoohna.shohna.ui.home.ui.cart.CartModel

class MyCartRecyclerViewAdapter (private var dataList: List<CartModel>, private val context: Context?) : RecyclerView.Adapter<MyCartRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CheckoutMycartItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = dataList[position]
        holder.bind(dataModel)

    }


    class ViewHolder(private var binding: CheckoutMycartItemRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CartModel) {
            binding.model = item
            binding.executePendingBindings()

        }

    }
}
