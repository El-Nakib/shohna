package com.shoohna.shohna.ui.home.ui.home

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.shohna.ui.home.MainActivity
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.ProductListRowBinding
import com.shoohna.shohna.pojo.model.ProductListModel

class ProductListRecyclerViewAdapter (private var dataList: List<ProductListModel>, private val context: Context?) : RecyclerView.Adapter<ProductListRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ProductListRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel= dataList[position]
        holder.bind(dataModel)

    }


    class ViewHolder(private var binding: ProductListRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ProductListModel) {
            binding.model = item
            binding.executePendingBindings()
            binding.mainConstraintLayoutId.setOnClickListener {
                Navigation.findNavController(itemView).navigate(R.id.action_homeFragment_to_productDetailsFragment)
                (itemView.context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE

            }
        }

    }

}