package com.shoohna.shohna.ui.welcome.ui.verifyCode

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentVerifyCodeBinding

/**
 * A simple [Fragment] subclass.
 */
class VerifyCodeFragment : Fragment() {

    lateinit var binding:FragmentVerifyCodeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentVerifyCodeBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
        val viewModel = ViewModelProvider(this).get(VerifyCodeViewModel::class.java)
        binding.vm = viewModel

//        viewModel._otp1.observe(viewLifecycleOwner, Observer {
//            if(it.isNotEmpty())
//                binding.editText11.requestFocus()
//
//        })
//        viewModel._otp2.observe(viewLifecycleOwner, Observer {
//            if(it.isNotEmpty())
//                binding.editText12.requestFocus()
//        })
//        viewModel._otp3.observe(viewLifecycleOwner, Observer {
//            if(it.isNotEmpty())
//                binding.editText13.requestFocus()
//        })
//        viewModel._otp4.observe(viewLifecycleOwner, Observer {
//            if(it.isNotEmpty())
//                binding.editText14.requestFocus()
//        })
//        viewModel._otp5.observe(viewLifecycleOwner, Observer {
//            if(it.isNotEmpty())
//                binding.editText15.requestFocus()
//        })

        binding.verifyBtnId.setOnClickListener {
            view?.let { Navigation.findNavController(it).navigate(R.id.action_verifyCodeFragment_to_changePasswordFragment) }
        }

        return binding.root

    }

}
