package com.shoohna.shohna.ui.home.ui.favorite

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.shohna.databinding.FavoriteViewRowBinding
import com.shoohna.shohna.pojo.model.ProductListModel
import com.shoohna.shohna.ui.home.ui.notification.NotificationModel


class FavoriteRecyclerViewAdapter  (private var dataList: List<ProductListModel>, private val context: Context?) : RecyclerView.Adapter<FavoriteRecyclerViewAdapter.ViewHolder>() {

    private var items: List<ProductListModel>? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FavoriteViewRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = dataList[position]
        holder.bind(dataModel)

    }

//
//    fun setFavoriteList(items: MutableLiveData<List<ProductListModel?>>) {
//        this.items = items.value
//        notifyDataSetChanged()
//    }


    class ViewHolder(private var binding: FavoriteViewRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ProductListModel) {
            binding.model = item
            binding.executePendingBindings()

        }

    }
}
