package com.shoohna.shohna.ui.home.ui.checkOutProcess.shipping

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentShippingBinding

/**
 * A simple [Fragment] subclass.
 */
class ShippingFragment : Fragment() {
    lateinit var binding: FragmentShippingBinding

//    private var stepList = ArrayList<StepBean>()
//    private var step1 : StepBean = StepBean("Cart",1)
//    private var step2 : StepBean = StepBean("Shipping",0)
//    private var step3 : StepBean = StepBean("Payment",-1)
//    private var step4 : StepBean = StepBean("Reciet",-1)
    var shippingList = ArrayList<ShippingModel>()
    lateinit var shippingAdapter: ShippingRecyclerViewAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentShippingBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner
//        setUpStepView()

        binding.nextBtnId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_shippingFragment_to_paymentWayFragment) }
        }

        shippingAdapter= ShippingRecyclerViewAdapter(shippingList, context?.applicationContext)
        binding.shippingRecycvlerViewId.adapter = shippingAdapter
        addData()
        return binding.root
    }

//    fun setUpStepView()
//    {
//        stepList.add(step1)
//        stepList.add(step2)
//        stepList.add(step3)
//        stepList.add(step4)
//
//        binding.stepViewId.setStepViewTexts(stepList).setTextSize(13)
//            .setStepsViewIndicatorCompletedLineColor(resources.getColor(R.color.pink))
//            .setStepsViewIndicatorUnCompletedLineColor(resources.getColor(R.color.grey))
//            .setStepViewComplectedTextColor(resources.getColor(R.color.pink))
//            .setStepViewUnComplectedTextColor(resources.getColor(R.color.pink))
//            .setStepsViewIndicatorCompleteIcon(resources.getDrawable(R.drawable.ic_step_completed))
//            .setStepsViewIndicatorDefaultIcon(resources.getDrawable(R.drawable.ic_step_after))
//            .setStepsViewIndicatorAttentionIcon(resources.getDrawable(R.drawable.ic_step_current))
//    }

    private fun addData() {
        shippingList.add(ShippingModel("Ahmed El-Nakib","ابها الجديده \n شارع الفن\nابها \nالسعوديه"))
        shippingList.add(ShippingModel("Ahmed El-Nakib","ابها الجديده \n شارع الفن\nابها \nالسعوديه"))
        shippingList.add(ShippingModel("Ahmed El-Nakib","ابها الجديده \n شارع الفن\nابها \nالسعوديه"))
        shippingList.add(ShippingModel("Ahmed El-Nakib","ابها الجديده \n شارع الفن\nابها \nالسعوديه"))
        shippingList.add(ShippingModel("Ahmed El-Nakib","ابها الجديده \n شارع الفن\nابها \nالسعوديه"))
        shippingAdapter.notifyDataSetChanged()

    }



}
