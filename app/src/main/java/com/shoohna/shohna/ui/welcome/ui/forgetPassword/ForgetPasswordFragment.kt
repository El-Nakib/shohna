package com.shoohna.shohna.ui.welcome.ui.forgetPassword

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentForgetPasswordBinding

/**
 * A simple [Fragment] subclass.
 */
class ForgetPasswordFragment : Fragment() {

    lateinit var binding:FragmentForgetPasswordBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentForgetPasswordBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner

        binding.sendBtnId.setOnClickListener {
            view?.let { Navigation.findNavController(it).navigate(R.id.action_forgetPasswordFragment_to_verifyCodeFragment) }
        }

        return binding.root
    }

}
