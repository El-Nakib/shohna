package com.shoohna.shohna.ui.home.ui.home

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.shoohna.shohna.ui.home.ui.home.HomeFragmentDirections
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentHomeBinding
import com.shoohna.shohna.pojo.model.ProductListModel
import com.shoohna.shohna.ui.home.ui.productDetails.ProductDetailsRecyclerViewAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.bottom_sheet_layout.*
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {
    lateinit var binding:FragmentHomeBinding
    var dataProductList = ArrayList<ProductListModel>()
    var dataOffersList = ArrayList<ProductListModel>()
    var dataCategorieList = ArrayList<FilterModel>()
    var dataList = ArrayList<ProductListModel>()
    lateinit var dataAdapter: ProductDetailsRecyclerViewAdapter
    lateinit var dataProductAdapter: ProductListRecyclerViewAdapter
    lateinit var dataOffersAdapter: ProductListRecyclerViewAdapter
    lateinit var dataCategorieAdapter: CategorieRecyclerViewAdapter
//    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner
        activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE
        Picasso.get().load("https://www.deutschland.de/sites/default/files/styles/crop_page/public/media/image/germany-children-living-education-school-growing-up.jpg?h=bd83f997&itok=wtCuyCpt").into(binding.imageViewId)
//        val model: HomeViewModel by viewModels()
//        model.tintCost.observe(activity!!, Observer {
//            Log.i("liveData",it.toString())
//        })

//        bottomSheetBehavior = BottomSheetBehavior.from<ConstraintLayout>(bottomSheet)

//        binding.btnBackId.setOnClickListener {
//            activity?.onBackPressed()
//        }

        binding.notificationImgId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_homeFragment_to_notificationFragment) }
            activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
        }

        binding.messangerImgId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_homeFragment_to_chatFragment) }
            activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE

        }

        dataAdapter= ProductDetailsRecyclerViewAdapter(dataList, context?.applicationContext)
        binding.productDetailsRecyclerViewId.adapter = dataAdapter



        dataProductAdapter= ProductListRecyclerViewAdapter(dataProductList, context?.applicationContext)
        binding.bestSaleRecyclerViewId.adapter = dataProductAdapter
        binding.highRelatedRecyclerViewId.adapter = dataProductAdapter

        dataOffersAdapter= ProductListRecyclerViewAdapter(dataOffersList, context?.applicationContext)
        binding.offersRecyclerViewId.adapter = dataOffersAdapter


        dataCategorieAdapter= CategorieRecyclerViewAdapter(dataCategorieList, context?.applicationContext)
        binding.categorieRecyclerViewId.adapter = dataCategorieAdapter

        binding.bestSaleTxtViewId.setOnClickListener {
            val action =
                HomeFragmentDirections.actionHomeFragmentToProductFragment(
                    resources.getString(R.string.bestSale)
                )
            view?.let { it1 -> Navigation.findNavController(it1).navigate(action)
            }
        }
//        binding.searchEditTextID.setOnFocusChangeListener { _, hasFocus ->
//            if(hasFocus)
//                activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
//        }

        binding.filterImgId.setOnClickListener {
//            slideUpDownBottomSheet()
            showBottomSheetDialogFragment()
        }

//        Log.i("navHight1", activity!!.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint).height.toString())
//        Log.i("navHight2", activity!!.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint).layoutParams.height.toString())
//        Log.i("navHight2", activity!!.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint).layoutParams.height.toString())

        addData()


        return binding.root
    }

    fun  addData()
    {
        dataProductList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","بالون مطاطي","30 ريال","1 Km","",false))
        dataProductList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","بالون مطاطي","30 ريال","1 Km","",false))
        dataProductList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","بالون مطاطي","30 ريال","1 Km","",false))
        dataProductList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","بالون مطاطي","30 ريال","1 Km","",false))
        dataProductList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","بالون مطاطي","30 ريال","1 Km","",false))
        dataProductAdapter.notifyDataSetChanged()



        dataOffersList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","بالون مطاطي","30 ريال","1 Km","10%",true))
        dataOffersList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","بالون مطاطي","30 ريال","1 Km","10%",true))
        dataOffersList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","بالون مطاطي","30 ريال","1 Km","10%",true))
        dataOffersList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","بالون مطاطي","30 ريال","1 Km","10%",true))
        dataOffersList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","بالون مطاطي","30 ريال","1 Km","10%",true))
        dataOffersList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","بالون مطاطي","30 ريال","1 Km","10%",true))
        dataOffersAdapter.notifyDataSetChanged()


        dataCategorieList.add(FilterModel("فريش"))
        dataCategorieList.add(FilterModel("اعياد ميلاد"))
        dataCategorieList.add(FilterModel("ورود"))
        dataCategorieList.add(FilterModel("جديد"))
        dataCategorieList.add(FilterModel("فريش"))
        dataCategorieList.add(FilterModel("اعياد ميلاد"))
        dataCategorieList.add(FilterModel("ورود"))
        dataCategorieList.add(FilterModel("جديد"))
        dataCategorieList.add(FilterModel("اعياد ميلاد"))
        dataCategorieList.add(FilterModel("ورود"))
        dataCategorieAdapter.notifyDataSetChanged()

        dataList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","","","","",false))
        dataList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","","","","",false))
        dataList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","","","","",false))
        dataList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","","","","",false))
        dataList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","","","","",false))
        dataList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","","","","",false))
        dataAdapter.notifyDataSetChanged()
    }


//    private fun slideUpDownBottomSheet() {
//        var bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
//        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback()
//        {
//            override fun onSlide(p0: View, p1: Float) {
//            }
//
//            override fun onStateChanged(p0: View, newState: Int) {
//                when (newState) {
//                    BottomSheetBehavior.STATE_COLLAPSED -> {
//                        activity?.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE
//
//                    }
//                    BottomSheetBehavior.STATE_HIDDEN -> {
//                        activity?.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE
//
//                    }
//                    BottomSheetBehavior.STATE_EXPANDED -> {
//                        activity?.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
//
//                    }
//                    BottomSheetBehavior.STATE_DRAGGING -> {
//
//                    }
//                    BottomSheetBehavior.STATE_SETTLING -> {
//
//                    }
//                }
//            }
//
//        })
//
////        var homeHeigth :Int = activity?.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint)?.layoutParams!!.height
//        if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
//            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
//            activity?.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
//        } else {
//            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED;
//            activity?.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE
//
//        }
//    }

    private fun showBottomSheetDialogFragment() {
        val bottomSheetFragment = FilterBottomSheet()
        activity?.supportFragmentManager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
    }
}
