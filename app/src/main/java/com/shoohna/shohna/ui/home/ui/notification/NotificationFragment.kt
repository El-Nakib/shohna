package com.shoohna.shohna.ui.home.ui.notification

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentNotificationBinding

/**
 * A simple [Fragment] subclass.
 */
class NotificationFragment : Fragment() {

    lateinit var binding: FragmentNotificationBinding
    var notificationViewList = ArrayList<NotificationModel>()
    lateinit var notificationViewAdapter: NotificationRecyclerViewAdapter


    var data= MutableLiveData<List<NotificationModel>>()
//    lateinit  var da:List<NotificationModel>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentNotificationBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
        val model = ViewModelProvider(this).get(NotificationViewModel::class.java)
        model.getNotificationList().observe(viewLifecycleOwner, Observer {notificationList ->
            data.value=notificationList
//            da=notificationList!!
//            Recycler_view.adapter=Adapter(this,data)
            binding.notificationRecyclerViewId.adapter = NotificationRecyclerViewAdapter(data, context?.applicationContext)

        })
//        notificationViewAdapter= NotificationRecyclerViewAdapter(notificationViewList, context?.applicationContext)
//        binding.notificationRecyclerViewId.adapter = notificationViewAdapter

//        addData()

        return binding.root
    }

    private fun addData() {
        notificationViewList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
        notificationViewList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
        notificationViewList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
        notificationViewList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
        notificationViewList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
        notificationViewList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
        notificationViewList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
        notificationViewList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
        notificationViewAdapter.notifyDataSetChanged()

    }

}
