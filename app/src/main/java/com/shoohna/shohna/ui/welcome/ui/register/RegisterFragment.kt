package com.shoohna.shohna.ui.welcome.ui.register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentRegisterBinding

/**
 * A simple [Fragment] subclass.
 */
class RegisterFragment : Fragment() {

    lateinit var binding: FragmentRegisterBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentRegisterBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner

        binding.signInTxtViewId.setOnClickListener {
            view?.let { Navigation.findNavController(it).navigate(R.id.action_registerFragment_to_loginFragment) }
        }

        return binding.root
    }

}
