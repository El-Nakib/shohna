package com.shoohna.shohna.ui.home.ui.chat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentChatBinding
import com.shoohna.shohna.ui.home.ui.chat.MessageItemUi.Companion.TYPE_APPLICATION_MESSAGE
import com.shoohna.shohna.ui.home.ui.chat.MessageItemUi.Companion.TYPE_MY_MESSAGE
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class ChatFragment : Fragment() {
    lateinit var binding: FragmentChatBinding
    lateinit var currentTime:String
    var chatList = ArrayList<MessageItemUi>()
    lateinit var chatAdapter: ChatAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentChatBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner

        binding.btnBackId.setOnClickListener { activity?.onBackPressed() }

        chatAdapter= ChatAdapter(chatList)
        binding.chatRecyclerViewId.adapter = chatAdapter

        currentTime = SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date())
        addData()

        binding.sendBtnId.setOnClickListener {
            sendMessage(binding.messageId.text.toString())
        }

        return binding.root
    }

    private fun sendMessage(message:String) {
        if(message.trim().isNotEmpty()) {
            currentTime = SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date())
            chatList.add(MessageItemUi(message, currentTime, TYPE_MY_MESSAGE))
            chatAdapter.notifyDataSetChanged()
            binding.messageId.text.clear()
//            binding.chatRecyclerViewId.smoothScrollToPosition(MessageItemUi - 1)
        }
        else
            Snackbar.make(binding.chatConstraintId, resources.getString(R.string.emptyMessage), Snackbar.LENGTH_SHORT).show();

//            Toast.makeText(activity,resources.getString(R.string.emptyMessage),Toast.LENGTH_SHORT).show()
    }

    fun addData()
    {
        chatList.add(MessageItemUi("Hello Sir :D ", currentTime,TYPE_APPLICATION_MESSAGE))
        chatList.add(MessageItemUi("How can i help you ?", currentTime,TYPE_APPLICATION_MESSAGE))
        chatAdapter.notifyDataSetChanged()
    }

}
