package com.shoohna.shohna.ui.home.ui.moreSetting

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentMoreBinding
import com.shoohna.shohna.databinding.FragmentMoreSettingBinding
import com.squareup.picasso.Picasso
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class MoreSettingFragment : Fragment(), AdapterView.OnItemSelectedListener {
    lateinit var binding: FragmentMoreSettingBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentMoreSettingBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner
        binding.spinner.onItemSelectedListener = this
        activity?.let { ArrayAdapter.createFromResource(it, R.array.language, android.R.layout.simple_spinner_item)
            .also { adapter -> adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                binding.spinner.adapter = adapter } }

        Picasso.get()
            .load("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTF3uiLXJ65UrAvjG6vUIVWJ3gVZVjj8yvOO51mI3sAFooiUDS3")
            .into(binding.circleImageView)

//        binding.spinner.setOnItemClickListener { parent, view, position, id ->
//            if(position == 0){
//                setLocate("en")
//                activity?.recreate()
//            }
//            else if(position == 1) {
//                setLocate("ar")
//                activity?.recreate()
//            }
//        }

//        binding.spinner.onItemSelectedListener = object :  AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
////                Toast.makeText(this@MainActivity, getString(R.string.selected_item) + " " + "" + languages[position], Toast.LENGTH_SHORT).show()
//
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>) {
//                // write code to perform some action
//            }
//        }


        return binding.root
    }

    private fun setLocate(Lang: String) {

        val locale = Locale(Lang)

        Locale.setDefault(locale)

        val config = Configuration()

        config.locale = locale
        activity?.resources?.updateConfiguration(config, activity!!.resources.displayMetrics)

        val editor = activity?.getSharedPreferences("Settings", Context.MODE_PRIVATE)?.edit()
        editor?.putString("My_Lang", Lang)
        editor?.apply()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (position == 1) {
            setLocate("en")
            activity?.recreate()
        }
        if (position == 2) {
            setLocate("ar")
            activity?.recreate()
        }
    }
}



