package com.shoohna.shohna.ui.home.ui.productDetails

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.shohna.databinding.ProductDetailsColorItemRowBinding

class ColorProductDetailsRecyclerViewAdapter (private var dataList: List<ColorProductDetailsModel>, private val context: Context?) : RecyclerView.Adapter<ColorProductDetailsRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ProductDetailsColorItemRowBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel= dataList[position]
        holder.bind(dataModel)

    }


    class ViewHolder(private var binding: ProductDetailsColorItemRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ColorProductDetailsModel) {
            binding.model = item
            binding.executePendingBindings()

        }

    }

}