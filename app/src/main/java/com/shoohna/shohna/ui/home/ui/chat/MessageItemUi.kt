package com.shoohna.shohna.ui.home.ui.chat

class MessageItemUi (val content:String,val time:String,val messageType:Int ){
    companion object {
        const val TYPE_MY_MESSAGE = 0
        const val TYPE_APPLICATION_MESSAGE = 1
    }
}

//https://medium.com/swlh/kotlin-chat-tutorial-the-clean-way-fca8f754aeb3