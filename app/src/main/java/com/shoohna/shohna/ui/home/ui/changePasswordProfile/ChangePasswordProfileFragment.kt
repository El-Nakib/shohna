package com.shoohna.shohna.ui.home.ui.changePasswordProfile

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shoohna.shohna.R

/**
 * A simple [Fragment] subclass.
 */
class ChangePasswordProfileFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_password_profile, container, false)
    }

}
