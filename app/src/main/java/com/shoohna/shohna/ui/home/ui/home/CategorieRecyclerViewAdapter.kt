package com.shoohna.shohna.ui.home.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.shohna.databinding.CategorieItemRowBinding

class CategorieRecyclerViewAdapter (private var dataList: List<FilterModel>, private val context: Context?) : RecyclerView.Adapter<CategorieRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CategorieItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel= dataList[position]
        holder.bind(dataModel)

    }


    class ViewHolder(private var binding: CategorieItemRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: FilterModel) {
            binding.model = item
            binding.executePendingBindings()

        }

    }

}