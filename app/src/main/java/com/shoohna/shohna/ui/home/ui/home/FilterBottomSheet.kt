package com.shoohna.shohna.ui.home.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.BottomSheetLayoutBinding
import com.shoohna.shohna.ui.home.ui.checkOutProcess.reciet.RecietModel
import kotlinx.android.synthetic.main.bottom_sheet_layout.view.*

class FilterBottomSheet : BottomSheetDialogFragment() {
    lateinit var binding:BottomSheetLayoutBinding
    //val model: HomeViewModel by viewModels()
    var filterList = ArrayList<FilterModel>()
    lateinit var filterAdapter: FilterRecyclerViewAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = BottomSheetLayoutBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

        val viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

//        https://www.youtube.com/watch?v=5qlIPTDE274
        binding.vm = viewModel
        viewModel.tintCost.observe(viewLifecycleOwner, Observer {
            Log.i("observe",it.toString())
        })

        binding.recyclerView.layoutManager = GridLayoutManager(this.context, 2, RecyclerView.HORIZONTAL, false)
        filterAdapter= FilterRecyclerViewAdapter(filterList, context?.applicationContext)
        binding.recyclerView.adapter = filterAdapter

        binding.rangeSeekBar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            binding.leftRangeTxtViewId.text = minValue.toString()
            binding.rigthRangeTxtViewId.text = maxValue.toString()
        }

        addData()

        return binding.root
    }

    private fun addData() {
        filterList.add(FilterModel("بالون مطاط"))
        filterList.add(FilterModel("بالون قصدير"))
        filterList.add(FilterModel("حروف"))
        filterList.add(FilterModel("مواليد"))
        filterList.add(FilterModel("ادوات حافلات"))
        filterList.add(FilterModel("ورود"))
        filterList.add(FilterModel("هدايا"))
        filterList.add(FilterModel("زواج"))
        filterAdapter.notifyDataSetChanged()

    }





}

