package com.shoohna.shohna.ui.home.ui.productDetails

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.shohna.databinding.ProductDetailsRowBinding
import com.shoohna.shohna.pojo.model.ProductListModel

class ProductDetailsRecyclerViewAdapter (private var dataList: List<ProductListModel>, private val context: Context?) : RecyclerView.Adapter<ProductDetailsRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ProductDetailsRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel= dataList[position]
        holder.bind(dataModel)

    }


    class ViewHolder(private var binding: ProductDetailsRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ProductListModel) {
            binding.model = item
            binding.executePendingBindings()

        }

    }

}