package com.shoohna.shohna.ui.home.ui.notification

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.shohna.databinding.NotificationItemRowBinding
import java.lang.String


class NotificationRecyclerViewAdapter  (val data: LiveData<List<NotificationModel>>, private val context: Context?) : RecyclerView.Adapter<NotificationRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            NotificationItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return data.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data.value!![position])

    }

    class ViewHolder(private var binding: NotificationItemRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: NotificationModel) {
            binding.model = item
            binding.executePendingBindings()
//            binding.mainConstraintLayoutId.setOnClickListener {
//                Navigation.findNavController(itemView).navigate(R.id.action_homeFragment_to_productDetailsFragment)
//                (itemView.context as MainActivity).findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
//
//            }
        }

    }

}