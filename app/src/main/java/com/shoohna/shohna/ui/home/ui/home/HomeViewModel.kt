package com.shoohna.shohna.ui.home.ui.home

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shoohna.shohna.pojo.model.ProductListModel

class HomeViewModel : ViewModel() {


    private var _tintTopRated = MutableLiveData<Boolean>(false)
    val tintTopRated: LiveData<Boolean> = _tintTopRated
    
    private var _tintNearestMe = MutableLiveData<Boolean>(false)
    val tintNearestMe: LiveData<Boolean> = _tintNearestMe

    private var _tintCost = MutableLiveData<Boolean>(false)
    val tintCost: LiveData<Boolean> = _tintCost



    fun allowTintTop(view: View)
    {
        _tintTopRated.value = true
        _tintNearestMe.value = false
        _tintCost.value = false
    }

    fun allowTintNearestMe(view: View)
    {
        _tintTopRated.value = false
        _tintNearestMe.value = true
        _tintCost.value = false
    }

    fun allowTintCost(view: View)
    {
        _tintTopRated.value = false
        _tintNearestMe.value = false
        _tintCost.value = true
    }
}