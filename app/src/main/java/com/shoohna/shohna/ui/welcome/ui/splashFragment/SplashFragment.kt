package com.shoohna.shohna.ui.welcome.ui.splash

import ApiClient.sharedHelper
import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentSplashBinding
import com.shoohna.shohna.ui.home.MainActivity
import com.shoohna.shohna.ui.welcome.WelcomeActivity
import com.shoohna.shohna.util.base.Constants
import com.shoohna.shohna.util.base.SharedHelper
import com.shoohna.shohna.util.base.BaseFragment
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class SplashFragment : Fragment() {

    val SPLASH_TIME :Long = 2000
    lateinit var binding: FragmentSplashBinding
    private val PERMISSIONS_REQUEST : Int = 1

    lateinit var baseFragment: BaseFragment
    lateinit var viewModel:SplashViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSplashBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
        viewModel = ViewModelProvider(this).get(SplashViewModel::class.java)

        val editor = context?.getSharedPreferences("Settings", Context.MODE_PRIVATE)?.edit()
        setLocate("ar")
        editor?.putString("My_Lang", "ar")
        editor?.apply()


        baseFragment= BaseFragment()


//        if(sharedHelper.getKey(activity!!,Constants.getToken())?.isNotEmpty()!!){
//            viewModel.getUserLanguage(activity!!)
//        }


//        if(checkPermissions()) {
//            askForPermissions()
//        }
//
//        askForPermissions()
        var x : SharedHelper = SharedHelper()
        var token : String? =""
        token = activity?.let { x.getKey(it, Constants.getToken()) }

        if (token.isNullOrEmpty() || token.equals(""))
        {
            Handler().postDelayed({
                view?.let { Navigation.findNavController(it).navigate(R.id.action_splashFragment_to_welcomeFragment) }
            }, SPLASH_TIME)
        }
        else
        {
            val sharedHelper = SharedHelper()
            sharedHelper.putKey(activity!!  , "OPEN" , "OPEN")
            val intent : Intent = Intent(context , MainActivity::class.java)
            context!!.startActivity(intent)
            (context as Activity).finish()
        }
        return  binding.root
    }

    private fun setLocate(Lang: String) {

        val locale = Locale(Lang)

        Locale.setDefault(locale)

        val config = Configuration()

        config.locale = locale
        activity?.resources?.updateConfiguration(config, activity!!.resources.displayMetrics)

        val editor = activity?.getSharedPreferences("Settings", Context.MODE_PRIVATE)?.edit()
        editor?.putString("My_Lang", Lang)
        editor?.apply()
    }




    //TODO we need to check why not working from base fragment
//    fun checkPermissions() : Boolean
//    {
//        return (activity?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.ACCESS_FINE_LOCATION) } != PackageManager.PERMISSION_GRANTED
//                && activity?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.READ_EXTERNAL_STORAGE) } != PackageManager.PERMISSION_GRANTED
//                && activity?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.WRITE_EXTERNAL_STORAGE) } != PackageManager.PERMISSION_GRANTED
//                && activity?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.CAMERA) } != PackageManager.PERMISSION_GRANTED
//                && activity?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.INTERNET) } != PackageManager.PERMISSION_GRANTED)
//    }
//
//    fun askForPermissions()
//    {
//        activity?.let {
//            ActivityCompat.requestPermissions(
//                it, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION
//                    ,Manifest.permission.READ_EXTERNAL_STORAGE
//                    ,Manifest.permission.WRITE_EXTERNAL_STORAGE
//                    ,Manifest.permission.CAMERA
//                    ,Manifest.permission.INTERNET)
//                ,PERMISSIONS_REQUEST)
//            Log.i("InsideBaseFragment1","true")
//
//        }
//        Log.i("InsideBaseFragment2","true")
//    }
//
//    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        when (requestCode)
//        {
//            PERMISSIONS_REQUEST -> {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.isNotEmpty()
//                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
//                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
//                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
//                    && grantResults[3] == PackageManager.PERMISSION_GRANTED
//                    && grantResults[4] == PackageManager.PERMISSION_GRANTED)
//                {
//                    Toast.makeText(activity,resources.getString(R.string.permissionGranted), Toast.LENGTH_SHORT).show()
//                } else {
//                    Toast.makeText(activity,resources.getString(R.string.permissionDenied), Toast.LENGTH_SHORT).show()
//
//                }
//                return
//            }
//        }
//    }


}
