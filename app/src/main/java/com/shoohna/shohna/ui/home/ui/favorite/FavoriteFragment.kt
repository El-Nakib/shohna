package com.shoohna.shohna.ui.home.ui.favorite

import android.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import com.shoohna.shohna.databinding.FragmentFavoriteBinding
import com.shoohna.shohna.pojo.model.ProductListModel
import com.shoohna.shohna.ui.home.ui.notification.NotificationModel
import com.squareup.picasso.Picasso


class FavoriteFragment : Fragment() {

    lateinit var binding: FragmentFavoriteBinding
    private var favoriteViewList = ArrayList<ProductListModel>()
    private lateinit var favoriteViewAdapter: FavoriteRecyclerViewAdapter
//    lateinit var favoriteViewModel: FavoriteViewModel
//    lateinit var favoriteViewModelFactory: FavoriteViewModelFactory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentFavoriteBinding.inflate(layoutInflater, container, false)
//        favoriteViewModelFactory = FavoriteViewModelFactory(getData())
//        favoriteViewModel = ViewModelProvider(this,favoriteViewModelFactory).get(FavoriteViewModel::class.java)
//        binding.vm = favoriteViewModel

        binding.lifecycleOwner

        favoriteViewAdapter= FavoriteRecyclerViewAdapter(favoriteViewList,activity)
        binding.favoriteViewRecyclerViewId.adapter = favoriteViewAdapter

        addData()
//        getData()
        return binding.root
    }

    private fun addData()
    {
        favoriteViewList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false))
        favoriteViewList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false))
        favoriteViewList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false))
        favoriteViewList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false))
        favoriteViewList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false))
        favoriteViewAdapter.notifyDataSetChanged()
    }


//    private fun getData(): MutableLiveData<ProductListModel> {
//        val model = MutableLiveData<ProductListModel>()
//        val product1 = ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false)
//        val product2 = ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false)
//        val product3 = ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false)
//        val product4 = ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false)
//
//        val list: MutableList<ProductListModel> = ArrayList()
//        list.add(product1)
//        list.add(product2)
//        list.add(product3)
//        list.add(product4)
//        val productListModel = ProductListModel("","","","","",false,list)
//        model.value = productListModel
//        return model
//    }

}
