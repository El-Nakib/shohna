package com.shoohna.shohna.ui.home.ui.cart

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.shohna.databinding.CartItemRowBinding

class CartRecyclerViewAdapter (private var dataList: List<CartModel>, private val context: Context?) : RecyclerView.Adapter<CartRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CartItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = dataList[position]
        holder.bind(dataModel)

    }


    class ViewHolder(private var binding: CartItemRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CartModel) {
            binding.model = item
            binding.executePendingBindings()

        }

    }
}
