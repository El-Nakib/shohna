package com.shoohna.shohna.ui.home.ui.checkOutProcess.myCart

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.get
import androidx.navigation.Navigation
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentCheckoutProcessBinding
import com.shoohna.shohna.ui.home.ui.cart.CartModel

/**
 * A simple [Fragment] subclass.
 */
class MyCartFragment : Fragment() {

    lateinit var binding: FragmentCheckoutProcessBinding
//    private var stepList = ArrayList<StepBean>()
//
//    private var step1 : StepBean = StepBean("Cart",0)
//    private var step2 : StepBean = StepBean("Shipping",-1)
//    private var step3 : StepBean = StepBean("Payment",-1)
//    private var step4 : StepBean = StepBean("Reciet",-1)

    var cartList = ArrayList<CartModel>()
    lateinit var cartAdapter: MyCartRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentCheckoutProcessBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner


        cartAdapter= MyCartRecyclerViewAdapter(cartList, context?.applicationContext)
        binding.myCartRecycvlerViewId.adapter = cartAdapter


//        setUpStepView()

        addData()

        binding.nextBtnId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_checkoutProcessFragment_to_shippingFragment) }
        }



        return binding.root
    }

//    fun setUpStepView()
//    {
//        stepList.add(step1)
//        stepList.add(step2)
//        stepList.add(step3)
//        stepList.add(step4)
//
//        binding.stepViewId.setStepViewTexts(stepList).setTextSize(13)
//            .setStepsViewIndicatorCompletedLineColor(resources.getColor(R.color.pink))
//            .setStepsViewIndicatorUnCompletedLineColor(resources.getColor(R.color.grey))
//            .setStepViewComplectedTextColor(resources.getColor(R.color.pink))
//            .setStepViewUnComplectedTextColor(resources.getColor(R.color.pink))
//            .setStepsViewIndicatorCompleteIcon(resources.getDrawable(R.drawable.ic_step_completed))
//            .setStepsViewIndicatorDefaultIcon(resources.getDrawable(R.drawable.ic_step_after))
//            .setStepsViewIndicatorAttentionIcon(resources.getDrawable(R.drawable.ic_step_current))
//    }


//
//    fun activateStateTwo()
//    {
//        step1.state = 1
//        step2.state = 0
//        step3.state = -1
//        step4.state = -1
//        binding.stepViewId.setStepViewTexts(stepList).setTextSize(14)
//
//    }
//
//    fun activateStateThree()
//    {
//        step1.state = 1
//        step2.state = 1
//        step3.state = 0
//        step4.state = -1
//        binding.stepViewId.setStepViewTexts(stepList).setTextSize(14)
//
//    }
//
//    fun activateStateFour()
//    {
//        step1.state = 1
//        step2.state = 1
//        step3.state = 1
//        step4.state = 0
//        binding.stepViewId.setStepViewTexts(stepList).setTextSize(14)
//
//    }


    private fun addData() {
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartList.add(CartModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22"))
        cartAdapter.notifyDataSetChanged()

    }

}
