package com.shoohna.shohna.ui.home

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.ActivityMainBinding
import com.squareup.picasso.Picasso

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    private lateinit var nav: NavController
    private lateinit var sharedPreferences: SharedPreferences
    var language :String? = ""



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE)
        language = sharedPreferences.getString("My_Lang", "")

        if (language.equals("ar")) {
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL;
        } else {
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR;
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
//        binding.vm = viewModel
//        viewModel.isShown.value = true
        nav = Navigation.findNavController(this,R.id.nav_host_fragment)
//        setUpController()

        iconHomeClicked()

        binding.bottomHomeConstraintId.setOnClickListener { iconHomeClicked() }
        binding.bottomCartConstraintId.setOnClickListener { iconCardClicked() }
        binding.bottomFavoriteConstraintId.setOnClickListener { iconFavoriteClicked() }
        binding.bottomMoreConstraintId.setOnClickListener { iconMoreClicked() }
        binding.bottomProfileImgId.setOnClickListener { iconProfileClicked() }
        binding.bottomNavigationConstraint.setOnClickListener { Log.i("bottomConstraint","clicked") }

        Picasso.get().load("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTF3uiLXJ65UrAvjG6vUIVWJ3gVZVjj8yvOO51mI3sAFooiUDS3").into(binding.bottomProfileImgId)

    }

//    private fun setUpController() {
//
//        binding.bottomNavigationConstraint.setNavigationChangeListener { view, position ->
//            if(position == 1) {
////                viewModel.isShown.value = true
//                if(nav.graph.startDestination != nav.currentDestination?.id) {
//                    nav.popBackStack(R.id.homeFragment,false)
//                    nav.navigate(R.id.action_homeFragment_to_cartFragment)
//                }
//                else
//                {
//                    nav.navigate(R.id.action_homeFragment_to_cartFragment)
//                }
//            }
//            if(position == 2) {
//
//                if(nav.graph.startDestination != nav.currentDestination?.id) {
//                    nav.popBackStack(R.id.homeFragment,false)
//                    nav.navigate(R.id.action_homeFragment_to_favoriteFragment)
//                }
//                else
//                {
//                    nav.navigate(R.id.action_homeFragment_to_favoriteFragment)
//                }
//            }
//            if(position == 3) {
//
//                if(nav.graph.startDestination != nav.currentDestination?.id) {
//                    nav.popBackStack(R.id.homeFragment,false)
//                    nav.navigate(R.id.action_homeFragment_to_profileFragment)
//                }
//                else
//                {
//                    nav.navigate(R.id.action_homeFragment_to_profileFragment)
//                }
//            }
//            if(position == 4) {
//
//                if(nav.graph.startDestination != nav.currentDestination?.id) {
//                    nav.popBackStack(R.id.homeFragment,false)
//                    nav.navigate(R.id.action_homeFragment_to_moreFragment)
//                }
//                else
//                {
//                    nav.navigate(R.id.action_homeFragment_to_moreFragment)
//                }
//            }
//            if(position == 0) {
//                nav.popBackStack(R.id.homeFragment,false)
//            }
//
//        }
//
//    }

    override fun onBackPressed() {
        super.onBackPressed()
//        binding.bottomNavigationConstraint.setCurrentActiveItem(0)
        iconHomeClicked()
    }

    private fun iconHomeClicked()
    {
        binding.bottomHomeId.setBackgroundResource(R.drawable.ic_bottom_home_active)
        binding.bottomCartId.setBackgroundResource(R.drawable.ic_bottom_cart_inactive)
        binding.bottomFavoriteId.setBackgroundResource(R.drawable.ic_bottom_favorite_inactive)
        binding.bottomMoreId.setBackgroundResource(R.drawable.ic_bottom_more_inactive)
        nav.popBackStack(R.id.homeFragment,false)

    }

    private fun iconCardClicked()
    {
        binding.bottomHomeId.setBackgroundResource(R.drawable.ic_bottom_home_inactive)
        binding.bottomCartId.setBackgroundResource(R.drawable.ic_bottom_cart_active)
        binding.bottomFavoriteId.setBackgroundResource(R.drawable.ic_bottom_favorite_inactive)
        binding.bottomMoreId.setBackgroundResource(R.drawable.ic_bottom_more_inactive)
        if(nav.graph.startDestination != nav.currentDestination?.id) {
            nav.popBackStack(R.id.homeFragment,false)
            nav.navigate(R.id.action_homeFragment_to_cartFragment)
        } else { nav.navigate(R.id.action_homeFragment_to_cartFragment) }
    }

    private fun iconFavoriteClicked()
    {
        binding.bottomHomeId.setBackgroundResource(R.drawable.ic_bottom_home_inactive)
        binding.bottomCartId.setBackgroundResource(R.drawable.ic_bottom_cart_inactive)
        binding.bottomFavoriteId.setBackgroundResource(R.drawable.ic_bottom_favorite_active)
        binding.bottomMoreId.setBackgroundResource(R.drawable.ic_bottom_more_inactive)
        if(nav.graph.startDestination != nav.currentDestination?.id) {
            nav.popBackStack(R.id.homeFragment,false)
            nav.navigate(R.id.action_homeFragment_to_favoriteFragment)
        } else { nav.navigate(R.id.action_homeFragment_to_favoriteFragment) }
    }

    private fun iconProfileClicked()
    {
        binding.bottomHomeId.setBackgroundResource(R.drawable.ic_bottom_home_inactive)
        binding.bottomCartId.setBackgroundResource(R.drawable.ic_bottom_cart_inactive)
        binding.bottomFavoriteId.setBackgroundResource(R.drawable.ic_bottom_favorite_inactive)
        binding.bottomMoreId.setBackgroundResource(R.drawable.ic_bottom_more_inactive)
        if(nav.graph.startDestination != nav.currentDestination?.id) {
            nav.popBackStack(R.id.homeFragment,false)
            nav.navigate(R.id.action_homeFragment_to_profileFragment)
        } else { nav.navigate(R.id.action_homeFragment_to_profileFragment) }
    }

    private fun iconMoreClicked()
    {
        binding.bottomHomeId.setBackgroundResource(R.drawable.ic_bottom_home_inactive)
        binding.bottomCartId.setBackgroundResource(R.drawable.ic_bottom_cart_inactive)
        binding.bottomFavoriteId.setBackgroundResource(R.drawable.ic_bottom_favorite_inactive)
        binding.bottomMoreId.setBackgroundResource(R.drawable.ic_bottom_more_active)
        if(nav.graph.startDestination != nav.currentDestination?.id) {
                    nav.popBackStack(R.id.homeFragment,false)
                    nav.navigate(R.id.action_homeFragment_to_moreFragment)
        } else { nav.navigate(R.id.action_homeFragment_to_moreFragment) }
    }



}
