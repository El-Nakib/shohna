package com.shoohna.shohna.ui.home.ui.profile

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.Navigation
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentFavoriteBinding
import com.shoohna.shohna.databinding.FragmentProfileBinding
import com.squareup.picasso.Picasso

/**binding.profileImageId
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {
    lateinit var binding: FragmentProfileBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentProfileBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner

        Picasso.get().load("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTF3uiLXJ65UrAvjG6vUIVWJ3gVZVjj8yvOO51mI3sAFooiUDS3").into(binding.profileImageId)
        binding.editConstraintId.setOnClickListener {
            binding.changePasswordTxtId.visibility = View.VISIBLE
            binding.bottomConstraintId.visibility = View.GONE
            binding.editConstraintId.visibility = View.GONE
            binding.correctImgId.visibility = View.VISIBLE
            binding.dontSaveImgId.visibility = View.VISIBLE
            binding.addImageBtnId.visibility = View.VISIBLE

        }

        binding.changePasswordTxtId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_profileFragment_to_changePasswordProfileFragment) }
            activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
        }

        binding.correctImgId.setOnClickListener {
            binding.changePasswordTxtId.visibility = View.GONE
            binding.bottomConstraintId.visibility = View.VISIBLE
            binding.editConstraintId.visibility = View.VISIBLE
            binding.correctImgId.visibility = View.GONE
            binding.dontSaveImgId.visibility = View.GONE
            binding.addImageBtnId.visibility = View.GONE
        }
        return binding.root
    }

}
