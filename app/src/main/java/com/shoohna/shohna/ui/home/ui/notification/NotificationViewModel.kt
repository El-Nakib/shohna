package com.shoohna.shohna.ui.home.ui.notification

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NotificationViewModel : ViewModel() {
    private var notificationList: MutableLiveData<List<NotificationModel>>? = null
    internal fun getNotificationList(): MutableLiveData<List<NotificationModel>> {
        if (notificationList == null) {
            notificationList = MutableLiveData()
            loadFruits()
        }
        return notificationList as MutableLiveData<List<NotificationModel>>
    }

    private fun loadFruits() {
        // do async operation to fetch users
        val myHandler = Handler()
        myHandler.postDelayed({
            val notificationStringList = ArrayList<NotificationModel>()
            notificationStringList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
            notificationStringList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
            notificationStringList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
            notificationStringList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
            notificationStringList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
            notificationStringList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
            notificationStringList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))
            notificationStringList.add(NotificationModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Baby Dress","Lorem ipsum dolor sit amet"))

            //fruitList!!.setValue(fruitsStringList)//for foreground setValue
            notificationList!!.postValue(notificationStringList)//for background postValue

        }, 1000)

    }
}