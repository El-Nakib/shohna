package com.shoohna.shohna.ui.home.ui.more

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.navigation.Navigation
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentMoreBinding
import com.squareup.picasso.Picasso


class MoreFragment : Fragment() {
    lateinit var binding: FragmentMoreBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentMoreBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner

        Picasso.get().load("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTF3uiLXJ65UrAvjG6vUIVWJ3gVZVjj8yvOO51mI3sAFooiUDS3").into(binding.circleImageView)
        binding.logOutConstraintId.setOnClickListener {
            showExitAlert()
        }

        binding.settingConstraintId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_moreFragment_to_moreSettingFragment) }
        }

        binding.contactUsConstraintId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_moreFragment_to_contactUsFragment) }
        }

        binding.aboutUsConstraintId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_moreFragment_to_aboutUsFragment) }
        }


        return binding.root
    }

    private fun showExitAlert()
    {
        val alert: Dialog? = activity?.let { Dialog(it) }
        alert?.setContentView(R.layout.exit_alert_layout)
        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

        alert?.show()
    }


}
