package com.shoohna.shohna.ui.home.ui.checkOutProcess.paymentWay

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentPaymentWayBinding

/**
 * A simple [Fragment] subclass.
 */
class PaymentWayFragment : Fragment() {

    lateinit var binding: FragmentPaymentWayBinding
//    private var stepList = ArrayList<StepBean>()
//    private var step1 : StepBean = StepBean("Cart",1)
//    private var step2 : StepBean = StepBean("Shipping",1)
//    private var step3 : StepBean = StepBean("Payment",0)
//    private var step4 : StepBean = StepBean("Reciet",-1)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentPaymentWayBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner

//        setUpStepView()
        binding.nextBtnId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_paymentWayFragment_to_recietFragment) }
        }

        return binding.root
    }

//    fun setUpStepView()
//    {
//        stepList.add(step1)
//        stepList.add(step2)
//        stepList.add(step3)
//        stepList.add(step4)
//
//        binding.stepViewId.setStepViewTexts(stepList).setTextSize(13)
//            .setStepsViewIndicatorCompletedLineColor(resources.getColor(R.color.pink))
//            .setStepsViewIndicatorUnCompletedLineColor(resources.getColor(R.color.grey))
//            .setStepViewComplectedTextColor(resources.getColor(R.color.pink))
//            .setStepViewUnComplectedTextColor(resources.getColor(R.color.pink))
//            .setStepsViewIndicatorCompleteIcon(resources.getDrawable(R.drawable.ic_step_completed))
//            .setStepsViewIndicatorDefaultIcon(resources.getDrawable(R.drawable.ic_step_after))
//            .setStepsViewIndicatorAttentionIcon(resources.getDrawable(R.drawable.ic_step_current))
//    }

}
