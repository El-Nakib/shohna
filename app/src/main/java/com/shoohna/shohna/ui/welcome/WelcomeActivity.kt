package com.shoohna.shohna.ui.welcome

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.shoohna.shohna.R
import com.shoohna.shohna.util.base.Constants
import com.shoohna.shohna.util.base.LanguageHelper
import com.shoohna.shohna.util.base.SharedHelper
import java.util.*

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        loadLocate() // call LoadLocate

    }
    private fun loadLocate() {
        val sharedHelper = SharedHelper()
        val language : String? = sharedHelper.getKey(this , Constants.getProfileLang())

        if (!language.equals("")) {
            sharedHelper.putKey(this , Constants.getProfileLang() , "ar")
        }

        val languageHelper = LanguageHelper()
        languageHelper.ChangeLang(this.resources , language )
    }
}
