package com.shoohna.shohna.ui.home.ui.productDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.fragment.app.Fragment
import com.shoohna.shohna.R
import com.shoohna.shohna.databinding.FragmentProductDetailsBinding
import com.shoohna.shohna.pojo.model.ProductListModel


/**
 * A simple [Fragment] subclass.
 */
class ProductDetailsFragment : Fragment() {
    lateinit var binding:FragmentProductDetailsBinding
    var dataList = ArrayList<ProductListModel>()
    lateinit var dataAdapter: ProductDetailsRecyclerViewAdapter
    var colorList = ArrayList<ColorProductDetailsModel>()
    lateinit var colorAdapter: ColorProductDetailsRecyclerViewAdapter

    lateinit var vto : ViewTreeObserver
    var detailsHeigth : Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentProductDetailsBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner

//        vto = binding.include2.detailsConstraintId.viewTreeObserver

        binding.btnBackId.setOnClickListener {
            activity?.onBackPressed()
        }

        dataAdapter= ProductDetailsRecyclerViewAdapter(dataList, context?.applicationContext)
        binding.detailsImageRecyclerViewId.adapter = dataAdapter

        colorAdapter= ColorProductDetailsRecyclerViewAdapter(colorList, context?.applicationContext)
//        binding.include2.colorRecyclerViewId.adapter = colorAdapter
//        addData()
//       activity?.let {
//            binding.include2.detailsConstraintId.setOnTouchListener(object : OnSwipeTouchListener(it) {
//
//                override fun onSwipeTop() {
//                    super.onSwipeTop()
//                    Log.i("onSwipeTop","true")
//                    animationUp()
//                }
//
//                override fun onSwipeBottom() {
//                    super.onSwipeBottom()
//                    Log.i("onSwipeBottom","true")
//                    animationDown()
//                }
//
//                override fun onSwipeLeft() {
//                    super.onSwipeLeft()
//                    Log.i("onSwipeLeft","true")
//
//                }
//
//                override fun onSwipeRight() {
//                    super.onSwipeRight()
//                    Log.i("onSwipeRight","true")
//
//                }
//            })
//        }
//
//        vto.addOnGlobalLayoutListener {
//            Log.i("Height",binding.include2.detailsConstraintId.layoutParams.height.toString())
//            Log.i("Height",binding.include2.detailsConstraintId.height.toString())
//            detailsHeigth = binding.include2.footerConstraintId.height
//        }



        return binding.root
    }

    fun  addData()
    {
        dataList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","","","","",false))
        dataList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","","","","",false))
        dataList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","","","","",false))
        dataList.add(ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","","","","",false))
        dataAdapter.notifyDataSetChanged()

        colorList.add(ColorProductDetailsModel(resources.getColor(R.color.grey)))
        colorList.add(ColorProductDetailsModel(resources.getColor(R.color.pink)))
        colorList.add(ColorProductDetailsModel(resources.getColor(R.color.colorPrimaryDark)))
        colorList.add(ColorProductDetailsModel(resources.getColor(R.color.colorAccent)))
        colorList.add(ColorProductDetailsModel(resources.getColor(R.color.pink)))

        colorAdapter.notifyDataSetChanged()

    }

    fun animationDown()
    {
//        val scaleYAnimator = ObjectAnimator.ofFloat(binding.include2.detailsConstraintId, "translationY", detailsHeigth.toFloat())
//        scaleYAnimator.duration = 1000
//        val scaleYAnimator2 = ObjectAnimator.ofFloat(binding.detailsImageRecyclerViewId, "translationY", detailsHeigth.toFloat())
//        scaleYAnimator2.duration = 1000
//        scaleYAnimator.start()
//        scaleYAnimator2.start()
    }
    fun animationUp()
    {
//        val scaleYAnimator = ObjectAnimator.ofFloat(binding.include2.detailsConstraintId, "translationY",  detailsHeigth.toFloat(),0f)
//        scaleYAnimator.duration = 1000
//        val scaleYAnimator2 = ObjectAnimator.ofFloat(binding.detailsImageRecyclerViewId, "translationY",  detailsHeigth.toFloat(),0f)
//        scaleYAnimator2.duration = 1000
//        scaleYAnimator.start()
//        scaleYAnimator2.start()

    }



}
