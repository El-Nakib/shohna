package com.shoohna.shohna.pojo.response

data class LoginResponse (
    val email: String,
    val frist_name: String,
    val id: Int,
    val image: String,
    val last_name: String,
    val lat: String,
    val lng: String,
    val phone: String,
    val social: String,
    val status: String
)