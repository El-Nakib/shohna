package com.shoohna.shohna.pojo.request

import com.shoohna.shohna.pojo.response.LoginResponse


data class LoginRequest(val message: String, val status: Boolean, val token: String, val user: LoginResponse)

